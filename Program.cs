﻿using System.Diagnostics;
using System.Windows.Forms;
using System.Configuration;

namespace usbWatch
{
    class Program
    {
        static void Main()
        {
            /* get from app config */
            string DEVICE_ID = ConfigurationManager.AppSettings.Get("hardwareId");

            /* setup watcher */
            Watcher w = new Watcher(DEVICE_ID);
            w.DeviceFound += W_DeviceFound;
            w.Start();

            /* setup tray icon */
            TrayIcon ti = new TrayIcon(DEVICE_ID);
            ti.forceSwitch += Ti_forceSwitch;
            Application.Run();
        }

        private static void Ti_forceSwitch(Monitor m)
        {
            Switcher.switchMonitor(m);
        }

        private static void W_DeviceFound(object sender, USBEvent e)
        {
            Monitor which = e == USBEvent.Connect ? Monitor.Primary : Monitor.Secondary;
            Switcher.switchMonitor(which);
        }
    }
}
