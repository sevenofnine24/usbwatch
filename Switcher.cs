﻿using System.Diagnostics;

namespace usbWatch
{
    enum Monitor
    {
        Primary,
        Secondary
    }
    class Switcher
    {
        public static void switchMonitor(Monitor which)
        {
            string path = string.Format("D:\\Scripts\\both_{0}.bat", which == Monitor.Primary ? "primary" : "secondary");
            Process p = new Process();
            p.StartInfo = new ProcessStartInfo(path);
            p.StartInfo.WorkingDirectory = @"D:\Scripts";
            p.StartInfo.CreateNoWindow = true;
            p.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            p.Start();
        }
    }
}
