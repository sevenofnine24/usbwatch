﻿using System;
using System.Windows.Forms;
using System.Drawing;
using System.ComponentModel;

namespace usbWatch
{
    class TrayIcon
    {
        public delegate void forceSwitchDelegate(Monitor m);
        public event forceSwitchDelegate forceSwitch;

        private IContainer components;
        private NotifyIcon notifyIcon;
        private ContextMenu contextMenu;
        private MenuItem exitItem;
        private MenuItem idItem;
        private MenuItem forcePrimary;
        private MenuItem forceSecondary;

        public TrayIcon(string usbId)
        {
            this.components = new Container();
            this.contextMenu = new ContextMenu();
            this.exitItem = new MenuItem();
            this.idItem = new MenuItem();
            this.forcePrimary = new MenuItem();
            this.forceSecondary = new MenuItem();

            /* this shows usb ID */
            this.idItem.Index = 0;
            this.idItem.Text = usbId;

            /* force switch */
            this.forcePrimary.Index = 1;
            this.forcePrimary.Text = "Force primary";
            this.forcePrimary.Click += ForcePrimary_Click;
            this.forcePrimary.Index = 2;
            this.forceSecondary.Text = "Force secondary";
            this.forceSecondary.Click += ForceSecondary_Click;

            /* exit */
            this.exitItem.Index = 3;
            this.exitItem.Text = "Exit";
            this.exitItem.Click += new EventHandler(this.closeApp);

            /* add items */
            this.contextMenu.MenuItems.AddRange(new MenuItem[] { this.idItem, this.forcePrimary, this.forceSecondary, this.exitItem });
            this.contextMenu.MenuItems.Add(this.idItem);
            this.contextMenu.MenuItems.Add(this.exitItem);
            
            this.notifyIcon = new NotifyIcon(this.components);
            notifyIcon.ContextMenu = this.contextMenu;
            notifyIcon.Text = "usbWatch";
            notifyIcon.Icon = new Icon("mini_usb.ico");
            notifyIcon.Visible = true;
        }


        private void ForcePrimary_Click(object sender, EventArgs e)
        {
            this.forceSwitch?.Invoke(Monitor.Primary);
        }
        private void ForceSecondary_Click(object sender, EventArgs e)
        {
            this.forceSwitch?.Invoke(Monitor.Secondary);
        }

        private void closeApp(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
