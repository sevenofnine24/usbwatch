﻿using System;
using System.Management;

namespace usbWatch
{
    enum USBEvent
    {
        Connect,
        Disconnect
    }
    class Watcher
    {
        public string DEVICE_ID;
        public event EventHandler<USBEvent> DeviceFound;

        private WqlEventQuery weqQuery;
        private ManagementEventWatcher watcher;

        public Watcher(string DEVICE_ID)
        {
            this.DEVICE_ID = DEVICE_ID;

            this.weqQuery = new WqlEventQuery();
            this.weqQuery.EventClassName = "__InstanceOperationEvent";
            this.weqQuery.WithinInterval = new TimeSpan(0, 0, 3);
            this.weqQuery.Condition = @"TargetInstance ISA 'Win32_PnPEntity'";

            this.watcher = new ManagementEventWatcher();
            this.watcher.EventArrived += new EventArrivedEventHandler(watcher_EventArrived);
            this.watcher.Query = weqQuery;
        }

        public void Start()
        {
            this.watcher.Start();
        }

        private void watcher_EventArrived(object sender, EventArrivedEventArgs e)
        {
            foreach (PropertyData pdData in e.NewEvent.Properties)
            {
                object val = pdData.Value;
                if (val != null && val is ManagementBaseObject)
                {
                    bool deviceFound = false;
                    ManagementBaseObject mbo = pdData.Value as ManagementBaseObject;
                    foreach (PropertyData pdDataSub in mbo.Properties)
                    {
                        if (pdDataSub.Name == "DeviceID")
                        {
                            if (pdDataSub.Value.ToString() == this.DEVICE_ID)
                                deviceFound = true;
                        }
                            
                    }

                    if (deviceFound)
                    {
                        if (e.NewEvent.ClassPath.ClassName == "__InstanceCreationEvent")
                            this.DeviceFound?.Invoke(this, USBEvent.Connect);
                        else if (e.NewEvent.ClassPath.ClassName == "__InstanceDeletionEvent")
                            this.DeviceFound?.Invoke(this, USBEvent.Disconnect);
                    }
                }
            }
        }
    }
}
